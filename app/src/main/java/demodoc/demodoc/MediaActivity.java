package demodoc.demodoc;

import android.Manifest;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import demodoc.demodoc.MediaService.MediaBinder;

public class MediaActivity extends AppCompatActivity implements TextToSpeech.OnInitListener, MediaPlayer.OnCompletionListener {
    private String[] tutorialStrings = {
            "You have reached the beginning of the tutorial! Swipe left to hear the next tutorial message, " +
                    "or swipe down with three fingers to close the tutorial.",
            "Swipe two fingers down to switch between navigation mode and player mode",
            "While in navigation mode, double tap to begin playing the selected song or podcast",
            "While in player mode, double tap to pause and play the current song or podcast"
    };
    private int tutorialPosition = 0;

    private static ArrayList<Media> trackList;
    private int newTrackPos = -1;

    private SeekBar seekbar;
    private TextView txt1, txt2, txt3, txt4, txt5, NavTxt;
    private static TextToSpeech tts;

    private static MediaService mediaService;
    private Intent playIntent;
    boolean mediaBound = false;

    private static boolean isPaused = true;
    private boolean firstTime = true;
    private boolean shuffle = false;
    private boolean searchingDir = false;

    private static double startTime = 0;
    private static double stopTime = 0;
    private static int forwardTime = 5000;
    private static int backwardTime = 5000;

    private static Handler myHandler = new Handler();

    private boolean NAVIGATION_MODE = true;

    private boolean musicFolderEmpty = false;
    private boolean podcastFolderEmpty = false;

    private NavigationState currState = NavigationState.Music;
    private NavigationState tempState = NavigationState.Music;

    static public Vibrator vib;

    private MyGestureListener listener;

    public Gesture GestureRecognizer() {
        listener.mDetector = new GestureDetectorCompat(this, listener);
        listener.mDetector.setOnDoubleTapListener(listener);

        return listener.gesture;
    }

    //text to speech
    @Override
    public void onInit(int status) {
        checkDir();

        txt5 = (TextView) findViewById(R.id.textView1);
        if (status == TextToSpeech.SUCCESS) {
            Locale loc = tts.getLanguage();
            int result = tts.setLanguage(loc);
            if (result == TextToSpeech.LANG_MISSING_DATA ||
                    result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "this language is not supported");
            } else {
                if (!musicFolderEmpty) {
                    txt5.setText("Music");
                    currState = NavigationState.Music;
                    speak_without_interuption("Double-tap to play Music, swipe down with three " +
                            "fingers for the Tutorial");
                } else if (!podcastFolderEmpty) {
                    txt5.setText("Podcasts");
                    currState = NavigationState.Podcasts;
                    speak_without_interuption("Double-tap to play Podcasts, swipe down with three " +
                            "fingers for the Tutorial");
                } else {
                    //changed
                    txt5.setText("Music");
                    currState = NavigationState.Music;
                }
            }
        } else {
            Log.e("TTS", "initialization falied");
        }
    }

    public void checkDir() {
        File MusicFolder = new File(Environment.getExternalStorageDirectory() + "/Music");
        File PodcastFolder = new File(Environment.getExternalStorageDirectory() + "/Podcasts");
        File[] MusicContents = MusicFolder.listFiles();
        File[] PodcastContents = PodcastFolder.listFiles();

        if (!MusicFolder.exists() && !PodcastFolder.exists()) {
            speak_without_interuption("Music and Podcast folders have been created," +
                    "please add media before starting the app");
            MusicFolder.mkdir();
            PodcastFolder.mkdir();
            this.finish();
            System.exit(0);
        } else if (!MusicFolder.exists() && PodcastFolder.exists()) {
            MusicFolder.mkdir();
            speak_without_interuption("Music Folder created, please add media");
        } else if (!PodcastFolder.exists() && MusicFolder.exists()) {
            PodcastFolder.mkdir();
            speak_without_interuption("Podcasts Folder created, please add media");
        }

        if ((MusicContents == null || MusicContents.length == 0) &&
                (PodcastContents == null || PodcastContents.length == 0)) {
            speak_without_interuption("No media found, please add media to the Music and" +
                    "Podcast folders on your device before using the app");
        } else if ((MusicContents != null || MusicContents.length != 0) &&
                (PodcastContents == null || PodcastContents.length == 0)) {
            //  only give the user the option to enter MUSIC directory
            // see swipe left/right code for if(!empty) statements
            podcastFolderEmpty = true;
        } else if ((MusicContents != null && MusicContents.length == 0) &&
                (PodcastContents != null || PodcastContents.length != 0)) {
            //only give the user the option to enter PODCAST directory
            // see swipe left/right code for if(!empty) statements
            musicFolderEmpty = true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);

        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, 1234);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        tts = new TextToSpeech(this, this);

        txt1 = (TextView) findViewById(R.id.textView5);
        txt2 = (TextView) findViewById(R.id.textView6);
        txt3 = (TextView) findViewById(R.id.textView4);
        txt4 = (TextView) findViewById(R.id.textView2);
        NavTxt = (TextView) findViewById(R.id.NavText);
        NavTxt.setVisibility(View.VISIBLE);

        txt3.setText(" ");
        txt1.setText(" ");
        txt2.setText(" ");
        txt4.setText("Double-tap to play.");

        seekbar = (SeekBar) findViewById(R.id.seekBar);
        seekbar.setVisibility(View.INVISIBLE);
        seekbar.setClickable(false);

        //creating a list of tracks to play
        trackList = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1); // 1 can be any number, doesn't matter
                return;
            }
        }

        vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        listener = new MyGestureListener();
        this.GestureRecognizer();
    }

    //connect to the service
    private ServiceConnection mediaConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MediaBinder binder = (MediaBinder) service;
            mediaService = binder.getService();
            mediaService.setTrackList(trackList);
            mediaBound = true;
            mediaService.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mediaService.nextTrack();
                    handleTextDisplay();
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mediaBound = false;
            stopService(playIntent);
        }
    };

    public void getMediaFromDevice() {
        ContentResolver mediaResolver = getContentResolver();
        Uri mediaUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor mediaCursor;
        if (currState == NavigationState.Podcasts) {
            mediaCursor = mediaResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null,
                    MediaStore.Audio.Media.DATA + " like ? ",
                    new String[]{"%Podcasts%"}, null);
        } else if (currState == NavigationState.Music) {
            mediaCursor = mediaResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null,
                    MediaStore.Audio.Media.DATA + " like ? ",
                    new String[]{"%Music%"}, null);
        } else {
            //get all audio off device
            //this will mix podcasts and music together inside music
            mediaCursor = mediaResolver.query(mediaUri, null, null, null, null);
        }
        if (mediaCursor != null && mediaCursor.moveToFirst()) {
            //get columns
            int titleColumn = mediaCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = mediaCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media._ID);
            int artistColumn = mediaCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.ARTIST);
            int durationColumn = mediaCursor.getColumnIndex
                    (MediaStore.Audio.Media.DURATION);
            do {
                long thisId = mediaCursor.getLong(idColumn);
                String thisTitle = mediaCursor.getString(titleColumn);
                String thisArtist = mediaCursor.getString(artistColumn);
                double thisDuration = mediaCursor.getDouble(durationColumn);
                trackList.add(new Media(thisId, thisTitle, thisArtist, thisDuration));
            }
            while (mediaCursor.moveToNext());
            mediaCursor.close();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (playIntent == null) {
            playIntent = new Intent(this, MediaService.class);
            bindService(playIntent, mediaConnection, Context.BIND_AUTO_CREATE);
            startService(playIntent);
        }
    }

    @Override
    protected void onDestroy() {
        if (mediaBound) {
            unbindService(mediaConnection);
            mediaService = null;
            tts.shutdown();
            super.onDestroy();
        }

    }

    public void playTrack() {
        seekbar.setVisibility(View.VISIBLE);
        txt4.setText("Now Playing:");
        handleTextDisplay();
        mediaService.createTrack();
        handleTimeDisplay();
    }

    public void handleTimeDisplay() {
        stopTime = mediaService.getDuration();
        startTime = mediaService.getTimePosition();
        seekbar.setMax((int) stopTime);

        txt2.setText(String.format("%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes((long) stopTime),
                TimeUnit.MILLISECONDS.toSeconds((long) stopTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                stopTime)))
        );

        txt1.setText(String.format("%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                startTime)))
        );

        seekbar.setProgress((int) startTime);
        myHandler.postDelayed(UpdateTrackTime, 100);
    }

    private Runnable UpdateTrackTime = new Runnable() {
        public void run() {
            startTime = mediaService.getTimePosition();
            txt1.setText(String.format("%d min, %d sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) startTime)))
            );
            seekbar.setProgress((int) startTime);
            myHandler.postDelayed(this, 100);
        }
    };

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        mediaService.nextTrack();
        handleTextDisplay();
    }

    public void handleTextDisplay() {
        int currentTrack = mediaService.getTrackNum();
        String current_Title = trackList.get(currentTrack).getTitle();
        String current_Artist = trackList.get(currentTrack).getArtist();
        txt3.setText(current_Title + " by " + current_Artist);
        if (isPaused) {
            txt4.setText("Now Playing:");
        }
    }

    public static void speak(String text) {
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    public static void speak_without_interuption(String text) {
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        while (tts.isSpeaking()) {
            //mediaService.adjustVolume((float)0.1);
            // nice to have but messes with first install of app
            //don't do anything until done speaking
        }
        //mediaService.adjustVolume((float)1.0);
    }

    public static void handleTextToSpeech() {
        int currentTrack = mediaService.getTrackNum();
        String current_Title = trackList.get(currentTrack).getTitle();
        String current_Artist = trackList.get(currentTrack).getArtist();
        String text = "Now playing. " + current_Title + " by " + current_Artist;
        speak_without_interuption(text);
    }

    public static void scrubFwd(int clicks) {
        int temp = (int) startTime;
        if ((temp + forwardTime) <= stopTime) {
            startTime = startTime + forwardTime * clicks;
            mediaService.scrub((int) startTime);
        }
    }

    public static void scrubBwd(int clicks) {
        int temp = (int) startTime;
        if ((temp - backwardTime) > 0) {
            startTime = startTime + backwardTime * clicks;
            mediaService.scrub((int) startTime);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event == null) {
            return false;
        }
        if (event.getPointerCount() == 2) {
            listener.onDoubleFling(event);
        }
        if (event.getPointerCount() == 3) {
            listener.onTripleFling(event);
        } else {
            listener.mDetector.onTouchEvent(event);
        }
        if (listener.INIT_WHEEL) {
            listener.onWheelListener(event);
        }
        GestureFilter();
        return super.onTouchEvent(event);
    }

    public void GestureFilter() {
        Gesture g = listener.gesture;

        switch (g) {

            case WHEEL:
                int currTrack = mediaService.getTrackNum();
                int local_clicks = listener.CLICKS;
                if (local_clicks > 0) {
                    if (NAVIGATION_MODE) {
                        searchingDir = true;
                        if (local_clicks == 0) {
                            handleTextToSpeech();
                        }
                        newTrackPos = (listener.CLICKS + currTrack) % trackList.size();
                        Media newTrack = trackList.get(newTrackPos);
                        speak_without_interuption(newTrack.getTitle() + " by " +
                                newTrack.getArtist() +
                                ". Double tap to play");
                    } else {
                        scrubFwd(local_clicks);
                    }
                } else if (listener.CLICKS < 0) {
                    if (NAVIGATION_MODE) {
                        searchingDir = true;
                        newTrackPos = (currTrack + listener.CLICKS);
                        while (newTrackPos < 0) {
                            newTrackPos += trackList.size();
                        }
                        if (newTrackPos > trackList.size()) {
                            newTrackPos %= trackList.size();
                        }
                        Media newTrack = trackList.get(newTrackPos);
                        speak(newTrack.getTitle() + " by " + newTrack.getArtist() +
                                ". Double tap to play");
                    } else {
                        scrubBwd(local_clicks);
                    }
                }
                listener.CLICKS = 0;
                listener.gesture = Gesture.VOID;
                break;

            case DOUBLE_TAP:
                handleDoubleTap();
                listener.gesture = Gesture.VOID;
                if (searchingDir) {
                    searchingDir = false;
                }
                break;

            case FLING_LEFT:
                if (NAVIGATION_MODE) {
                    if (currState != NavigationState.Tutorial) {
                        if (!musicFolderEmpty && !podcastFolderEmpty) {
                            if (tempState == NavigationState.Music) {
                                tempState = NavigationState.Podcasts;
                                txt5.setText("Podcasts");
                                speak("Podcasts");
                            } else {
                                tempState = NavigationState.Music;
                                txt5.setText("Music");
                                speak("Music");
                            }
                        }
                    }
                } else {

                    if (currState != NavigationState.Tutorial) {
                        mediaService.nextTrack();
                        handleTextDisplay();
                        handleTimeDisplay();
                    }
                    // Tutorial fling left
                    // Go to next tutorial message
                    else {
                        tutorialPosition++;
                        if (tutorialPosition >= tutorialStrings.length) {
                            tutorialPosition = tutorialStrings.length - 1;
                            speak("You have reached the end of the tutorial! " +
                                    "Swipe right to hear the previous tutorial message," +
                                    "or swipe down with three fingers to close the tutorial.");
                        } else {
                            speak(tutorialStrings[tutorialPosition]);
                        }
                    }
                }
                if (searchingDir) {
                    searchingDir = false;
                }
                listener.gesture = Gesture.VOID;
                break;

            case FLING_RIGHT:
                if (NAVIGATION_MODE) {
                    if (!musicFolderEmpty && !podcastFolderEmpty) {
                        if (tempState == NavigationState.Music) {
                            tempState = NavigationState.Podcasts;
                            txt5.setText("Podcasts");
                            speak("Podcasts");
                        } else {
                            tempState = NavigationState.Music;
                            txt5.setText("Music");
                            speak("Music");
                        }
                    }
                } else {
                    if (currState != NavigationState.Tutorial) {
                        mediaService.nextTrack();
                        handleTextDisplay();
                        handleTimeDisplay();
                    }
                    // Tutorial fling right
                    // Go back to previous tutorial message
                    else {
                        tutorialPosition--;
                        if (tutorialPosition < 0) {
                            tutorialPosition = 0;
                            speak("You have reached the beginning of the tutorial! " +
                                    "Swipe left to hear the next tutorial message," +
                                    "or swipe down with three fingers to close the tutorial.");
                        } else {
                            speak(tutorialStrings[tutorialPosition]);
                        }
                    }

                }
                listener.gesture = Gesture.VOID;
                if (searchingDir) {
                    searchingDir = false;
                }
                break;


            case TRIPLE_FLING_DOWN:
                if (currState == NavigationState.Tutorial) {
                    if (!NAVIGATION_MODE) {
                        if (firstTime) {
                            NAVIGATION_MODE = false;
                            //tempState = NavigationState.Music;
                            currState = tempState;
                            txt5.setText("Music");
                            txt4.setText("Double-tap to Play");
                        } else {
                            if (mediaService.isPaused()) {
                                if (tempState == NavigationState.Music) {
                                    txt5.setText("Music");
                                } else {
                                    txt5.setText("Podcasts");
                                }
                                mediaService.startPlayer();
                                txt4.setText("Now Playing");

                                NAVIGATION_MODE = true;
                                currState = tempState;
                            }
                        }
                            speak("Closing Tutorial");

                    } else { //NAVIGATION MODE
                        if (mediaService.isPaused()) {
                            if (tempState == NavigationState.Music) {
                                txt5.setText("Music");
                            } else {
                                txt5.setText("Podcasts");
                            }
                            mediaService.startPlayer();
                            txt4.setText("Now Playing");
                        }
                        NAVIGATION_MODE = false;
                        currState = tempState;
                        speak("Closing Tutorial");
                    }
                } else { //if current state is podcast or music
                    if (!NAVIGATION_MODE) {
                        mediaService.pausePlayer();
                        txt4.setText("Paused");
                        NAVIGATION_MODE = false;
                        mediaService.pausePlayer();

                        tempState = currState;
                        currState = NavigationState.Tutorial;
                        NavTxt.setVisibility(View.INVISIBLE);
                        txt5.setText("Tutorial");
                        speak("Welcome to the Tutorial. Swipe left to access the next tutorial message");
                    }
                    else { //NAVIGATION MODE
                        if(firstTime){
                            txt4.setText("Swipe left to continue");
                        }else{
                            txt4.setText("Paused");
                        }
                        mediaService.pausePlayer();
                        NAVIGATION_MODE = false;
                        mediaService.pausePlayer();

                        tempState = currState;
                        currState = NavigationState.Tutorial;
                        NavTxt.setVisibility(View.INVISIBLE);
                        txt5.setText("Tutorial");
                        speak("Welcome to the Tutorial. Swipe left to access the next tutorial message");
                    }
                }
                listener.gesture = Gesture.VOID;
                break;


            case DOUBLE_FLING_DOWN:
                if (currState != NavigationState.Tutorial) {
                    if (NAVIGATION_MODE) {
                        NavTxt.setVisibility(View.INVISIBLE);
                        speak("Player Mode");
                        NAVIGATION_MODE = false;
                        if (firstTime) {
                            NAVIGATION_MODE = true;
                        }
                        if (currState == NavigationState.Music) {
                            currState = NavigationState.Music;
                            txt5.setText("Music");
                        } else if (currState == NavigationState.Podcasts) {
                            currState = NavigationState.Podcasts;
                            txt5.setText("Podcasts");
                        }
                        tempState = currState;

                    } else {
                        NavTxt.setVisibility(View.VISIBLE);
                        speak("Navigation Mode");
                        NAVIGATION_MODE = true;
                    }
                    listener.gesture = Gesture.VOID;
                    if (searchingDir) {
                        searchingDir = false;
                    }
                }
               listener.gesture = Gesture.VOID;
                break;

            case DOUBLE_FLING_RIGHT:
                if (currState != NavigationState.Tutorial) {
                    mediaService.setShuffle();
                    if (shuffle == false) {
                        speak("Media is now shuffled");
                        shuffle = true;
                    } else {
                        speak("Shuffle is off");
                        shuffle = false;
                    }
                    listener.gesture = Gesture.VOID;
                    if (searchingDir) {
                        searchingDir = false;
                    }
                }
                break;
            case DOUBLE_FLING_LEFT:
                if (currState != NavigationState.Tutorial) {
                    speak("Shuffle is off");
                    shuffle = false;
                    listener.gesture = Gesture.VOID;
                    if (searchingDir) {
                        searchingDir = false;
                    }
                }
                break;
            default:
                return;
        }
    }

    public void handleDoubleTap() {
        if (currState != NavigationState.Tutorial) {
            if (isPaused) {
                if (firstTime) { //initial double tap
                    if(NAVIGATION_MODE){
                        if(tempState == NavigationState.Music){
                            currState = NavigationState.Music;
                        }
                        if (tempState == NavigationState.Podcasts){
                            currState = NavigationState.Podcasts;
                        }
                    }
                    getMediaFromDevice();
                    handleTextToSpeech();
                    playTrack();
                    firstTime = false;
                    isPaused = false;
                    NAVIGATION_MODE = false;
                    NavTxt.setVisibility(View.INVISIBLE);
                } else if (NAVIGATION_MODE) {
                    handleDoubleTapInNav();
                } else {
                    txt4.setText("Now Playing:");
                    mediaService.startPlayer(); //unpause
                    isPaused = false;
                }
            } else { //not paused
                if (NAVIGATION_MODE) {
                    handleDoubleTapInNav();
                } else {
                    isPaused = true;
                    mediaService.pausePlayer();
                    txt4.setText("Paused:");
                    NAVIGATION_MODE = false;
                    NavTxt.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    //helper for handleDoubleTap
    public void handleDoubleTapInNav() {
        if (NAVIGATION_MODE) {
            if (searchingDir) {
                Log.i("SINGLE_TAP", "newTrackPos: " + newTrackPos);
                if (NAVIGATION_MODE &&
                        mediaService.getTrackListSize() > 0
                        && newTrackPos >= 0) {
                    mediaService.setTrack(newTrackPos);
                    listener.gesture = Gesture.VOID;
                    handleTextDisplay();
                    handleTimeDisplay();
                    searchingDir = false;
                }
                NAVIGATION_MODE = false;
                NavTxt.setVisibility(View.INVISIBLE);
            } else {
                if(tempState == NavigationState.Music){
                    currState = NavigationState.Music;
                }
                if (tempState == NavigationState.Podcasts){
                    currState = NavigationState.Podcasts;
                }
                mediaService.resetPlayer();
                trackList.clear();
                getMediaFromDevice();
                mediaService.setTrack(0);
                handleTextToSpeech();
                playTrack();
                NAVIGATION_MODE = false;
                NavTxt.setVisibility(View.INVISIBLE);
            }
        }
    }
}