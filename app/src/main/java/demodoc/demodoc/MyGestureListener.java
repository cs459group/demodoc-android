package demodoc.demodoc;

import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created by kaleb on 3/11/17.
 */
enum Gesture {
    FLING_RIGHT, FLING_LEFT, SINGLE_TAP, DOUBLE_TAP, DOUBLE_FLING_RIGHT, DOUBLE_FLING_LEFT,
    DOUBLE_FLING_DOWN, TRIPLE_FLING_DOWN, LONG_PRESS, WHEEL, VOID
}

public class MyGestureListener implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
    public GestureDetectorCompat mDetector;
    public Gesture gesture;
    private Section section;
    private enum Section {CENTER, NONE, TOP, RIGHT, BOTTOM, LEFT};
    public int CLICKS = 0;
    private final int SWIPE_THRESHOLD = 100;
    private boolean SWIPE = false;
    public boolean INIT_WHEEL = true;
    int wheelInnerRadius = 25;
    int wheelOuterRadius = 325;
    private boolean WHEEL = false;
    private float startX, startY;
    private float stopX, stopY;
    private final String DEBUG_TAG = "Gestures";
    private final int SWIPE_DISTANCE_THRESHOLD = 100;
    private final int SWIPE_VELOCITY_THRESHOLD = 100;

    public Section getSection(){ return section; }

    public boolean onTripleFling(MotionEvent event) {

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case (MotionEvent.ACTION_POINTER_DOWN):
                SWIPE = true;
                // sets start coord for double swiping using average of two fingers
                startX = (event.getX(0) + event.getX(1)) / 2;
                startY = (event.getY(0) + event.getY(1)) / 2;
                break;

            case (MotionEvent.ACTION_MOVE):
                if (SWIPE) {
                    stopX = (event.getX(0) + event.getX(1)) / 2;
                    stopY = (event.getY(0) + event.getY(1)) / 2;
                }
                break;

            case (MotionEvent.ACTION_POINTER_UP):
                SWIPE = false;
                if (Math.abs(startX - stopX) > SWIPE_THRESHOLD && Math.abs(startY - stopY) < 2 * SWIPE_THRESHOLD) {
                    // left or right swipe

                } else if (Math.abs(startY - stopY) > SWIPE_THRESHOLD && Math.abs(startX - stopX) < 2 * SWIPE_THRESHOLD) {
                    // down swipe
                    if (startY < stopY) {
                        gesture = Gesture.TRIPLE_FLING_DOWN;
                        Log.d("myGestures: ", "DoubleFlingDown");
                    }
                    // else : unused double swipe up
                }
                break;
        }
        return true;
    }

    public boolean onDoubleFling(MotionEvent event) {

        switch(event.getAction() & MotionEvent.ACTION_MASK){
            case (MotionEvent.ACTION_POINTER_DOWN) :
                SWIPE = true;
                // sets start coord for double swiping using average of two fingers
                startX = (event.getX(0) + event.getX(1))/2;
                startY = (event.getY(0) + event.getY(1))/2;
                break;

            case (MotionEvent.ACTION_MOVE) :
                if( SWIPE ) {
                    stopX = (event.getX(0) + event.getX(1)) / 2;
                    stopY = (event.getY(0) + event.getY(1)) / 2;
                }
                break;

            case (MotionEvent.ACTION_POINTER_UP) :
                SWIPE = false;
                if( Math.abs(startX - stopX) > SWIPE_THRESHOLD && Math.abs(startY - stopY) < 2*SWIPE_THRESHOLD ){
                    // left or right swipe
                    if( startX > stopX ){
                        gesture = Gesture.DOUBLE_FLING_RIGHT;
                        Log.d("myGestures: ", "DoubleFlingRight");
                    } else {
                        gesture = Gesture.DOUBLE_FLING_LEFT;
                        Log.d("myGestures: ", "DoubleFlingLeft");
                    }
                } else if ( Math.abs(startY - stopY) > SWIPE_THRESHOLD && Math.abs(startX - stopX) < 2*SWIPE_THRESHOLD){
                    // down swipe
                    if( startY < stopY ){
                        gesture = Gesture.DOUBLE_FLING_DOWN;
                        Log.d("myGestures: ", "DoubleFlingDown");
                    }
                    // else : unused double swipe up
                }
                break;
        }
        return true;
    }

    public boolean onWheelListener(MotionEvent event){
        int action = MotionEventCompat.getActionMasked(event);

        switch(event.getAction() & MotionEvent.ACTION_MASK) {
            case (MotionEvent.ACTION_DOWN):
                if (INIT_WHEEL) {
                    startX = event.getX();
                    startY = event.getY();
                    WHEEL = true;
                    section = Section.CENTER;
                    INIT_WHEEL = false;
                    gesture = Gesture.WHEEL;
                    //CLICKS = 0;
                }
                break;

            case (MotionEvent.ACTION_UP):
                WHEEL = false;
                Log.d("CLICKS: ", ((Number)CLICKS).toString());
                gesture = Gesture.WHEEL;
                break;

            case (MotionEvent.ACTION_MOVE):
                if (WHEEL) {
                    if (inTop(startX, startY, event.getX(), event.getY())) {
                        if (section == Section.CENTER) {
                            MediaActivity.vib.vibrate(100);
                        } else if(section == Section.LEFT){
                            MediaActivity.vib.vibrate(100);
                            //MediaActivity.scrubFwd();
                           CLICKS++;
                        } else if( section == Section.RIGHT){
                            MediaActivity.vib.vibrate(100);
                            //MediaActivity.scrubBwd();
                            CLICKS--;
                        }
                        section = Section.TOP;
                        Log.d("inSection ", "inTop");
                    } else if( inLeft( startX, startY, event.getX(), event.getY() )){
                        if (section == Section.CENTER) {
                            MediaActivity.vib.vibrate(100);
                        } else if(section == Section.BOTTOM){
                            MediaActivity.vib.vibrate(100);
                            //MediaActivity.scrubFwd();
                            CLICKS++;
                        } else if( section == Section.TOP){
                            MediaActivity.vib.vibrate(100);
                            //MediaActivity.scrubBwd();
                            CLICKS--;
                        }
                        section = Section.LEFT;
                        Log.d("inSection ", "inLeft");
                    } else if( inBottom( startX, startY, event.getX(), event.getY() )){
                        if (section == Section.CENTER) {
                            MediaActivity.vib.vibrate(100);
                        } else if(section == Section.RIGHT){
                            MediaActivity.vib.vibrate(100);
                            CLICKS++;
                            //MediaActivity.scrubFwd();
                        } else if( section == Section.LEFT){
                            MediaActivity.vib.vibrate(100);
                            CLICKS--;
                            //MediaActivity.scrubBwd();
                        }
                        section = Section.BOTTOM;
                        Log.d("inSection ", "inBottom");
                    } else if( inRight( startX, startY, event.getX(), event.getY() )){
                        if (section == Section.CENTER) {
                            MediaActivity.vib.vibrate(100);
                        } else if(section == Section.TOP){
                            MediaActivity.vib.vibrate(100);
                            CLICKS++;
                            //MediaActivity.scrubFwd();
                        } else if( section == Section.BOTTOM){
                            MediaActivity.vib.vibrate(100);
                            CLICKS--;
                            //MediaActivity.scrubBwd();
                        }
                        section = Section.RIGHT;
                        Log.d("inSection ", "inRight");;
                    }
                }
                break;
        }
        return true;
    }

    public boolean inTop(float startX, float startY, float curX, float curY){
        double theta = Math.toDegrees( Math.atan( ((curY-startY)/(curX-startX))) );
        double r =  Math.sqrt( ((curX-startX)*(curX-startX) + (curY-startY)*(curY-startY)) );
        //System.err.println("Theta = " + theta + " r = " + r);

        return ( (curY-startY) < 0 && (theta >= 70 || theta <= -70)
                && r >= wheelInnerRadius && r <= wheelOuterRadius);
    }

    public boolean inLeft(float startX, float startY, float curX, float curY){
        double theta = Math.toDegrees( Math.atan( ((curY-startY)/(curX-startX))) );
        double r =  Math.sqrt( ((curX-startX)*(curX-startX) + (curY-startY)*(curY-startY)) );
        //System.err.println("Theta = " + theta + " r = " + r);

        return ( (curX-startX) < 0 && theta >= -20 && theta <= 20
                && r >= wheelInnerRadius && r <= wheelOuterRadius);
    }

    public boolean inBottom(float startX, float startY, float curX, float curY){
        double theta = Math.toDegrees( Math.atan( ((curY-startY)/(curX-startX))) );
        double r =  Math.sqrt( ((curX-startX)*(curX-startX) + (curY-startY)*(curY-startY)) );
        //System.err.println("Theta = " + theta + " r = " + r);

        return ( (curY-startY) > 0 && (theta >= 70 || theta <= -70)
                && r >= wheelInnerRadius && r <= wheelOuterRadius);
    }

    public boolean inRight(float startX, float startY, float curX, float curY){
        double theta = Math.toDegrees( Math.atan( ((curY-startY)/(curX-startX))) );
        double r =  Math.sqrt( ((curX-startX)*(curX-startX) + (curY-startY)*(curY-startY)) );
        //System.err.println("Theta = " + theta + " r = " + r);

        return ( (curX-startX) > 0 && theta >= -20 && theta <= 20
                && r >= wheelInnerRadius && r <= wheelOuterRadius);
    }

    @Override
    public boolean onDown(MotionEvent event) {
        //Log.d(DEBUG_TAG,"onDown: " + event.toString());
        return true;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {
        float distanceX = event2.getX() - event1.getX();
        float distanceY = event2.getY() - event1.getY();
        Log.d("Fling", "Point Count" + event1.getPointerCount());
        if(event1.getPointerCount() != 1){

        }

        if (Math.abs(distanceX) > Math.abs(distanceY) && Math.abs(distanceX) >
                SWIPE_DISTANCE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
            if (distanceX > 0) {
                gesture = Gesture.FLING_RIGHT;
//                MainActivity.previousTrack();
            }else {
                gesture = Gesture.FLING_LEFT;
//                nextTrack();
                return true;
            }
        }
        return false;
    }

    @Override
    public void onLongPress(MotionEvent event) {
        Log.d(DEBUG_TAG, "onLongPress: " + event.toString());
        if( event.getX() < 1500 && event.getX() > 500 && event.getY() < 2000 && event.getY() > 500) {
            INIT_WHEEL = true;
            MediaActivity.vib.vibrate(100);

        }
        gesture = Gesture.LONG_PRESS;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event){
        Log.d(DEBUG_TAG, "onDoubleTap: " + event.toString());
        gesture = Gesture.DOUBLE_TAP;
        return true;
    }

    public boolean GetWheelState(){ return WHEEL;}

    @Override
    public boolean onDoubleTapEvent(MotionEvent event){
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2,
                            float distanceX, float distanceY){
        return false;
    }

    @Override
    public void onShowPress(MotionEvent event){}

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        gesture = Gesture.SINGLE_TAP;
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
        return false;
    }
}
