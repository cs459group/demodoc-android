package demodoc.demodoc;

/**
 * Created by Jake on 3/10/2017.
 */

public class Media {
    private long rawID;
    private String title;
    private String artist;
    private double duration;

    public Media(long rawID_, String title_, String artist_, double duration_){
        rawID = rawID_;
        title = title_;
        artist = artist_;
        duration = duration_;
    }

    public long getRawID(){return rawID;}
    public String getTitle(){return title;}
    public String getArtist(){return artist;}
    public double getDuration(){return duration;}
}
