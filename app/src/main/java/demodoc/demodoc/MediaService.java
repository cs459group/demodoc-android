package demodoc.demodoc;

import android.app.Service;
import java.util.ArrayList;

import android.content.ContentUris;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;

import android.content.Intent;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;
import java.util.Random;

/**
 * Created by Jake on 3/10/2017.
 */

public class MediaService extends Service implements MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {

    private static final String DEBUG_TAG_Service = "Service";
    public MediaPlayer mediaPlayer;
    private ArrayList<Media> media;
    private int trackNum;
    private final IBinder mediaBind = new MediaBinder();
    private String trackTitle;
    private String trackArtist;
    int shuffledPrevTrack;

    private boolean shuffle=false;
    private Random rand = new Random();
    private double duration = 100000;

//    public MediaPlayer getPlayer(){ return mediaPlayer; }

    @Override
    public void onCreate() {
        super.onCreate();
       // Toast.makeText(getApplicationContext(), "service in onCreate.", Toast.LENGTH_SHORT).show();
        trackNum = 0;
        mediaPlayer = new MediaPlayer();
        setServiceProperties();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        return START_NOT_STICKY;
    }

    public void setServiceProperties() {
        mediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnPreparedListener(this);
        //mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnErrorListener(this);
    }


    public void setTrackList(ArrayList<Media> allMedia) {
        media = allMedia;
    }

    public class MediaBinder extends Binder {
        MediaService getService() {
            return MediaService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mediaBind;
    }

    @Override
    public boolean onUnbind(Intent intent){
        mediaPlayer.stop();
        mediaPlayer.release();
        return false;
    }

    public void createTrack(){
        mediaPlayer.reset();
        //get track
        Media playMedia = media.get(trackNum);
        //get track info
        trackTitle = playMedia.getTitle();
        trackArtist = playMedia.getArtist();
        duration = playMedia.getDuration();
        Log.d(DEBUG_TAG_Service, "getting duration in create: " + String.valueOf(duration));
        long currTrack = playMedia.getRawID();
        //set uri
        Uri uri = ContentUris.withAppendedId(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                currTrack);
        Log.d("Media", "id=" + currTrack+ ", title="+trackTitle + ", artist=" + trackArtist);
        //set source
        try{
            mediaPlayer.setDataSource(getApplicationContext(), uri);
        }
        catch(Exception e){
            Log.e("MUSIC SERVICE", "Error setting data source", e);
        }
        mediaPlayer.prepareAsync();
    }


    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
    }

//    @Override
//    public void onCompletion(MediaPlayer mediaPlayer) {
//        //nextTrack();
//    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int a, int b) {
        mediaPlayer.reset();
        return true;
    }

    @Override
    public void onDestroy() {
        mediaPlayer.reset();
        stopForeground(true);
    }

    public int getTimePosition(){
        return mediaPlayer.getCurrentPosition();
    }

    public  int getTrackNum(){
        return trackNum;
    }

    public void setTrackNum(int otherTrackNum){this.trackNum = otherTrackNum;}

    public double getDuration(){
        return duration;
    }

    public void pausePlayer(){
        mediaPlayer.pause();
    }

    public boolean isPaused() {
        if(!mediaPlayer.isPlaying()) {
            return true;
        }else{
            return  false;
        }
    }

    public void scrub(int currentTime){
        mediaPlayer.seekTo(currentTime);
    }

    public void startPlayer(){
        mediaPlayer.start();
    }

    public void resetPlayer(){mediaPlayer.reset();}


    public void prevTrack(){
        if(shuffle){
            trackNum = shuffledPrevTrack;
        }else{
            trackNum--;
            if(trackNum < 0){
                trackNum = media.size()-1;
            }
        }
        createTrack();
    }


    public void nextTrack(){
        if(shuffle){
            int nextTrack = trackNum;
            while(nextTrack==trackNum){
                shuffledPrevTrack  = trackNum;
                nextTrack = rand.nextInt(media.size());
            }
            trackNum=nextTrack;
        }
        else{
            trackNum++;
            if(trackNum >= media.size()){
                trackNum=0;
            }
        }
        createTrack();
    }

    public void setTrack(int index){
        trackNum = index;
        createTrack();
    }

    public void setShuffle(){
        if(shuffle){
            shuffle=false;
        } else {
            shuffle=true;
        }
    }

    public int getTrackListSize(){
        return media.size();
    }

    public void adjustVolume(float volume){
        mediaPlayer.setVolume(volume, volume);
    }






}
