package demodoc.demodoc;

/**
 * Created by nicklaskenyon on 4/23/17.
 */

public enum NavigationState {
    Tutorial, Music, Podcasts
}
